# Using Terraform to automatically provision a VM in Azure

## Required Tools:

* Azure CLI: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli
* Terraform: https://www.terraform.io/downloads


## Preqrequisites

You need an Azure account and optionally create a service principle
that can be used by Terraform. You can authenticate against Azure
using the Azure CLI, it will launch a web browser for you to provide
your credentials, and then following a successful authentication will
print out some details about your subscription. Please take note of
the id. That is your subscription id:

```bash
$ az login
[
  {
    ...
    "id": "a0ad9dca-****-****-****-************",
    ...
  }
]
```

If you wish, you can create a service principle as shown below. It
should have the role "Contributor". The command will print out
details about the service principle you have just created. Take note
of all these details.

```bash
$ az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/a0ad9dca-****-****-****-************" -n TerraformTesting
{
  "appId": "********-****-****-****-************",
  "displayName": "TerraformTesting",
  "password": "*****~**********************************",
  "tenant": "********-****-****-****-************"
}
```

If we are using a service principle, then we need to set some
environment variables. We need the subscription id from the account
details, and the appId, password and tenant from the service
principle details.

This example is for Linux, adjust to suit Windows Command Prompt or
PowerShell if you prefer:

```bash
$ export ARM_SUBSCRIPTION_ID=<id from the account details>
$ export ARM_CLIENT_ID=<appID from the service principle details>
$ export ARM_CLIENT_SECRET=<password from the service principle details>
$ export ARM_TENANT_ID=<tenant from the service principle details>

```

## Instantiate Virtual Machine

Running terraform requires just a couple of steps, first inititialise
it, and let it download the extensions/modules that are required:

```bash
$ terraform init
```

And then, simply run:

```bash
$ terraform apply
```

It will prompt you for the password that you wish to use for the
virtual machine, print a plan of what it will do. Type 'yes' and
then watch terraform provision your new virtual machine and other
resources required to run it.

## Destroy your Virtual Machine

Once you are done, assuming you still have a valid session with
Azure (if you don't, just run `az login` again) run:

```bash
$ terraform destroy
```

And type yes to confirm that you want to remove all resources.

