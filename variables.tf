
variable "location" {
  description = "Where to provision the new machine."
  default     = "East US"
}

variable "machine_size" {
  description = "Machine Size SKU"
  default     = "Standard_D4_v3"
}

variable "admin_username" {
  description = "Windows Administrator Username"
  default     = "developer"
}

variable "admin_password" {
  description = "Windows Administrator Password"
  sensitive   = true
}

variable "source_address" {
  description = "Source IP address used for accessing this VM."
}

