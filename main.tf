provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "developer" {
  name     = "developer-resources"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "developer" {
  name                = "developer-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.developer.location
  resource_group_name = azurerm_resource_group.developer.name
}

resource "azurerm_subnet" "developer" {
  name                 = "developer-subnet"
  resource_group_name  = azurerm_resource_group.developer.name
  virtual_network_name = azurerm_virtual_network.developer.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "public_ip" {
  name                = "developer-public-ip"
  resource_group_name = azurerm_resource_group.developer.name
  location            = azurerm_resource_group.developer.location
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "developer" {
  name                          = "developer-nic"
  location                      = azurerm_resource_group.developer.location
  resource_group_name           = azurerm_resource_group.developer.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.developer.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.public_ip.id
  }
}

resource "azurerm_network_security_group" "developer" {
  name                = "developer-network-security-group"
  location            = azurerm_resource_group.developer.location
  resource_group_name = azurerm_resource_group.developer.name

  security_rule {
    name                       = "rdp"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    # Here you would put the public IP address of the network your laptop is connected to.
    source_address_prefix      = "${var.source_address}"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface_security_group_association" "developer" {
  network_interface_id      = azurerm_network_interface.developer.id
  network_security_group_id = azurerm_network_security_group.developer.id
}

resource "azurerm_windows_virtual_machine" "developer" {
  name                = "developer-vm"
  resource_group_name = azurerm_resource_group.developer.name
  location            = azurerm_resource_group.developer.location
  size                = "${var.machine_size}"
  admin_username      = "${var.admin_username}"
  admin_password      = "${var.admin_password}"
  network_interface_ids = [
    azurerm_network_interface.developer.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsDesktop"
    offer     = "Windows-10"
    sku       = "win10-21h2-pron"
    version   = "latest"
  }
}

resource "azurerm_virtual_machine_extension" "developer" {
  name                 = "InstallDeveloperTools"
  virtual_machine_id   = azurerm_windows_virtual_machine.developer.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  settings = <<SETTINGS
  {
    "fileUris": [
      "https://gitlab.com/pdgaskell-infra-code/azure-dev-machine/-/raw/main/resources/install_tools.ps1"
    ],
    "commandToExecute": "powershell -ExecutionPolicy Unrestricted -File install_tools.ps1"
  }
  SETTINGS
}

